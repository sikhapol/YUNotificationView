//
//  YUNotificationView.m
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 3/25/16.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

#import "YUNotificationView.h"

@implementation YUNotificationView

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self _init];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // Get status bar frame
    // Get content frame
}

#pragma mark - Private

- (void)_init {
    
}

@end


//self.notificationView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
////    self.notificationView.backgroundColor = [UIColor greenColor];
////    self.notificationView.alpha = 0.25;
//
//self.animator = [[UIDynamicAnimator alloc] init];

//[self.animator removeAllBehaviors];
//
//UIView *navBar = self.navigationController.navigationBar;
//self.notificationView.frame = CGRectMake(0, -64, CGRectGetWidth(navBar.frame), 64);
//
//UIView *greenView = [[UIView alloc] initWithFrame:self.notificationView.bounds];
//greenView.alpha = 0.05;
//greenView.backgroundColor = [UIColor whiteColor];
//[self.notificationView addSubview:greenView];
//[navBar addSubview:self.notificationView];
//navBar.backgroundColor = [UIColor redColor];
//
//UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[self.notificationView]];
//[self.animator addBehavior:gravity];
//
//UIPushBehavior *push = [[UIPushBehavior alloc] initWithItems:@[self.notificationView] mode:UIPushBehaviorModeInstantaneous];
//push.magnitude = 15;
//push.angle = M_PI / 2;
//[self.animator addBehavior:push];
//
//UIView *separator = findSeparator(navBar);
//
//UICollisionBehavior *collision = [[UICollisionBehavior alloc] initWithItems:@[self.notificationView]];
//[collision addBoundaryWithIdentifier:@"test" fromPoint:CGPointMake(0, 45.5) toPoint:CGPointMake(375, 45.5)];
//[self.animator addBehavior:collision];
//
//dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//    [self.animator removeAllBehaviors];
//    
//    UIPushBehavior *push = [[UIPushBehavior alloc] initWithItems:@[self.notificationView] mode:UIPushBehaviorModeInstantaneous];
//    push.magnitude = 20;
//    push.angle = M_PI / 2 * 3;
//    [self.animator addBehavior:push];
//});