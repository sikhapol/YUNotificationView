//
//  UINavigationBar+SeparatorView.h
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 3/22/16.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (SeparatorView)

@property (nonatomic, readonly) UIView *separatorView NS_AVAILABLE_IOS(7_0);

@end
