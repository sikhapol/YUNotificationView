//
//  YUNotificationView.h
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 3/25/16.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUNotificationView : UIView

@property (nonatomic) UIView *contentView;
@property (nonatomic) UIView *backgroundView;

@end
