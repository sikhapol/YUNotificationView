//
//  UINavigationBar+SeparatorView.m
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 3/22/16.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

#import "UINavigationBar+SeparatorView.h"

@implementation UINavigationBar (SeparatorView)

UIView *getSeparatorView(UIView *view) {
    if ([view isKindOfClass:[UIImageView class]] && CGRectGetHeight(view.bounds) <= 1) { // Separator view has height at most 1 pt.
        return view;
    }
    
    for (UIView *subView in view.subviews) {
        UIView *separatorView = getSeparatorView(subView);
        if (separatorView) {
            return separatorView;
        }
    }
    
    return nil;
}

- (UIView *)separatorView {
    NSUInteger backgroundViewIndex = [self.subviews indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return *stop = [obj isKindOfClass:NSClassFromString(@"_UINavigationBarBackground")];
    }];
    
    if (backgroundViewIndex == NSNotFound) { return nil; };
    
    return getSeparatorView(self.subviews[backgroundViewIndex]);
}

@end
