//
//  YUViewController.m
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 01/27/2016.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

#import "YUViewController.h"

@interface YUViewController ()

@property (nonatomic) UIView *notificationView;

@property (nonatomic) UIDynamicAnimator *animator;

@end

@implementation YUViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

@end
