//
//  YUAppDelegate.h
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 01/27/2016.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

@import UIKit;

@interface YUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
