//
//  main.m
//  YUNotificationView
//
//  Created by Sikhapol Saijit on 01/27/2016.
//  Copyright (c) 2016 Sikhapol Saijit. All rights reserved.
//

@import UIKit;
#import "YUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YUAppDelegate class]));
    }
}
