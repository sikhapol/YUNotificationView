# YUNotificationView

[![CI Status](http://img.shields.io/travis/Sikhapol Saijit/YUNotificationView.svg?style=flat)](https://travis-ci.org/Sikhapol Saijit/YUNotificationView)
[![Version](https://img.shields.io/cocoapods/v/YUNotificationView.svg?style=flat)](http://cocoapods.org/pods/YUNotificationView)
[![License](https://img.shields.io/cocoapods/l/YUNotificationView.svg?style=flat)](http://cocoapods.org/pods/YUNotificationView)
[![Platform](https://img.shields.io/cocoapods/p/YUNotificationView.svg?style=flat)](http://cocoapods.org/pods/YUNotificationView)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YUNotificationView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YUNotificationView"
```

## Author

Sikhapol Saijit, sikhapol@gmail.com

## License

YUNotificationView is available under the MIT license. See the LICENSE file for more info.
